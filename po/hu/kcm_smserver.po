#
# Tamas Szanto <tszanto@interware.hu>, 2000.
# Kristóf Kiszel <ulysses@kubuntu.org>, 2010, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4.2\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-14 00:48+0000\n"
"PO-Revision-Date: 2021-01-13 20:22+0100\n"
"Last-Translator: Kristóf Kiszel <kiszel.kristof@gmail.com>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.03.70\n"

#: kcmsmserver.cpp:49
#, kde-format
msgid ""
"<h1>Session Manager</h1> You can configure the session manager here. This "
"includes options such as whether or not the session exit (logout) should be "
"confirmed, whether the session should be restored again when logging in and "
"whether the computer should be automatically shut down after session exit by "
"default."
msgstr ""
"<h1>Munkamenetkezelő</h1> Itt lehet módosítani a munkamenetkezelés "
"beállításait. A beállítások közé tartozik például: megerősítés kérése "
"kijelentkezés előtt, a munkamenet állapotának visszatöltése "
"bejelentkezéskor, illetve hogy kijelentkezéskor automatikusan megtörténjen-e "
"a gép lezárása."

#: package/contents/ui/main.qml:25
#, kde-format
msgid "Failed to request restart to firmware setup: %1"
msgstr "Nem sikerült az újraindítás a firmware telepítéshez: %1"

#: package/contents/ui/main.qml:26
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the UEFI setup screen."
msgstr ""
"A számítógép a következő újrainduláskor az UEFI beállítóképernyő fog "
"megjelenni."

#: package/contents/ui/main.qml:27
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the firmware setup screen."
msgstr ""
"A számítógép a következő újrainduláskor a firmware-telepítő képernyő fog "
"megjelenni."

#: package/contents/ui/main.qml:32
#, kde-format
msgid "Restart Now"
msgstr "Újraindítás most"

#: package/contents/ui/main.qml:38
#, kde-format
msgid "General:"
msgstr "Általános:"

#: package/contents/ui/main.qml:39
#, fuzzy, kde-format
#| msgid "Confirm logout"
msgctxt "@option:check"
msgid "Confirm logout"
msgstr "Megerősítés kérése kijelentkezésnél"

#: package/contents/ui/main.qml:48
#, fuzzy, kde-format
#| msgid "Offer shutdown options"
msgctxt "@option:check"
msgid "Offer shutdown options"
msgstr "A lezárási lehetőségek megjelenítése"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Default leave option:"
msgstr "Alapértelmezett kilépési mód:"

#: package/contents/ui/main.qml:66
#, fuzzy, kde-format
#| msgid "End current session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "End current session"
msgstr "Munkamenet befejezése"

#: package/contents/ui/main.qml:76
#, fuzzy, kde-format
#| msgid "Restart computer"
msgctxt "@option:radio"
msgid "Restart computer"
msgstr "Számítógép újraindítása"

#: package/contents/ui/main.qml:86
#, fuzzy, kde-format
#| msgid "Turn off computer"
msgctxt "@option:radio"
msgid "Turn off computer"
msgstr "Számítógép kikapcsolása"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "When logging in:"
msgstr "Kijelentkezéskor:"

#: package/contents/ui/main.qml:103
#, fuzzy, kde-format
#| msgid "Restore manually saved session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last session"
msgstr "Kézzel mentett állapot visszatöltése"

#: package/contents/ui/main.qml:113
#, fuzzy, kde-format
#| msgid "Restore manually saved session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last manually saved session"
msgstr "Kézzel mentett állapot visszatöltése"

#: package/contents/ui/main.qml:123
#, fuzzy, kde-format
#| msgid "Start with an empty session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Start with an empty session"
msgstr "Indítás alapállapotban"

#: package/contents/ui/main.qml:135
#, kde-format
msgid "Don't restore these applications:"
msgstr "Ne állítsa vissza ezeket az alkalmazásokat:"

#: package/contents/ui/main.qml:152
#, kde-format
msgid ""
"Here you can enter a colon or comma separated list of applications that "
"should not be saved in sessions, and therefore will not be started when "
"restoring a session. For example 'xterm:konsole' or 'xterm,konsole'."
msgstr ""
"Itt lehet vesszővel vagy kettősponttal elválasztva felsorolni azokat a "
"programokat, amelyek állapotát nem kell elmenteni (ezért ezek "
"visszaállításkor nem fognak visszaállni korábbi állapotukba). Például: "
"\"xterm:konsole\" vagy \"xterm,konsole\"."

#: package/contents/ui/main.qml:161
#, fuzzy, kde-format
#| msgid "Enter UEFI setup on next restart"
msgctxt "@option:check"
msgid "Enter UEFI setup screen on next restart"
msgstr "UEFI beállítóképernyő indítása a következő újrainduláskor"

#: package/contents/ui/main.qml:162
#, fuzzy, kde-format
#| msgid "Enter firmware setup screen on next restart"
msgctxt "@option:check"
msgid "Enter firmware setup screen on next restart"
msgstr "Firmware-telepítő indítása a következő újrainduláskor"

#. i18n: ectx: label, entry (confirmLogout), group (General)
#: smserversettings.kcfg:9
#, kde-format
msgid "Confirm logout"
msgstr "Megerősítés kérése kijelentkezésnél"

#. i18n: ectx: label, entry (offerShutdown), group (General)
#: smserversettings.kcfg:13
#, kde-format
msgid "Offer shutdown options"
msgstr "A lezárási lehetőségek megjelenítése"

#. i18n: ectx: label, entry (shutdownType), group (General)
#: smserversettings.kcfg:17
#, kde-format
msgid "Default leave option"
msgstr "Alapértelmezett kilépési mód"

#. i18n: ectx: label, entry (loginMode), group (General)
#: smserversettings.kcfg:26
#, kde-format
msgid "On login"
msgstr "Bejelentkezéskor"

#. i18n: ectx: label, entry (excludeApps), group (General)
#: smserversettings.kcfg:30
#, kde-format
msgid "Applications to be excluded from session"
msgstr "Az állapotkezelésből kizárandó alkalmazások:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Kiszel Kristóf"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kiszel.kristof@gmail.com"

#~ msgid "Desktop Session"
#~ msgstr "Asztali munkamenet"

#~ msgid "Desktop Session Login and Logout"
#~ msgstr "Be- és kijelentkezés az asztali munkamenetbe"

#~ msgid "Copyright © 2000–2020 Desktop Session team"
#~ msgstr "Copyright © Az Asztali munkamenet fejlesztői, 2000-2020."

#~ msgid "Oswald Buddenhagen"
#~ msgstr "Oswald Buddenhagen"

#~ msgid "Carl Schwan"
#~ msgstr "Carl Schwan"

#~ msgid "Restore previous saved session"
#~ msgstr "Előző mentett munkamenet visszaállítása"

#~ msgid "UEFI Setup"
#~ msgstr "UEFI beállítás"

#~ msgid ""
#~ "Check this option if you want the session manager to display a logout "
#~ "confirmation dialog box."
#~ msgstr ""
#~ "Jelölje be ezt az opciót, ha azt szeretné, hogy megjelenjen egy "
#~ "megerősítést kérő párbeszédablak a kilépési parancs kiadása után."

#~ msgid "Conf&irm logout"
#~ msgstr "Megerősítés kérése &kijelentkezésnél"

#~ msgid "O&ffer shutdown options"
#~ msgstr "A le&zárási lehetőségek megjelenítése"

#~ msgid ""
#~ "Here you can choose what should happen by default when you log out. This "
#~ "only has meaning, if you logged in through KDM."
#~ msgstr ""
#~ "Itt lehet kiválasztani, hogy kijelentkezéskor mi történjen. Csak akkor "
#~ "van jelentősége, ha a grafikus felületen (a KDM-en) keresztül "
#~ "jelentkezett be."

#~ msgid "Default Leave Option"
#~ msgstr "Alapértelmezett kilépési mód"

#~ msgid ""
#~ "<ul>\n"
#~ "<li><b>Restore previous session:</b> Will save all applications running "
#~ "on exit and restore them when they next start up</li>\n"
#~ "<li><b>Restore manually saved session: </b> Allows the session to be "
#~ "saved at any time via \"Save Session\" in the K-Menu. This means the "
#~ "currently started applications will reappear when they next start up.</"
#~ "li>\n"
#~ "<li><b>Start with an empty session:</b> Do not save anything. Will come "
#~ "up with an empty desktop on next start.</li>\n"
#~ "</ul>"
#~ msgstr ""
#~ "<ul>\n"
#~ "<li><b>A legutóbbi állapot visszatöltése:</b> a rendszer megjegyzi a futó "
#~ "alkalmazásokat kilépés előtt, és azokat induláskor újból elindítja</li>\n"
#~ "<li><b>Kézzel mentett állapot visszatöltése: </b> a pillanatnyi állapot "
#~ "bármikor elmenthető a KDE menü \"Az állapot mentése\" parancsával. Ez azt "
#~ "jelenti, hogy az éppen futó alkalmazások az indításkor újból el fognak "
#~ "indulni.</li>\n"
#~ "<li><b>Indítás mindig alapállapotban:</b> a rendszer nem menti el a "
#~ "programok állapotát, a munkaasztal indításkor alaphelyzetbe kerül.</li>\n"
#~ "</ul>"

#~ msgid "On Login"
#~ msgstr "Bejelentkezéskor"

#~ msgid "Applications to be e&xcluded from sessions:"
#~ msgstr "Az állapotkezelésből k&izárandó alkalmazások:"

#~ msgid "Firmware Setup"
#~ msgstr "Firmware-telepítés"

#~ msgid ""
#~ "When the computer is restarted the next time, enter firmware setup screen "
#~ "(e.g. UEFI or BIOS setup)"
#~ msgstr ""
#~ "A firmware-telepítő képernyő megjelenítése a számítógép következő "
#~ "újraindulásakor (például UEFI vagy BIOS beállítás)"

#~ msgid "Session Manager"
#~ msgstr "Munkamenetkezelő"
