# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Yoann Laissus <yoann.laissus@gmail.com>, 2015.
# Simon Depiets <sdepiets@gmail.com>, 2019.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2020, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2021-10-21 18:56+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.08.1\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: contents/ui/ToolBoxContent.qml:269
#, kde-format
msgid "Choose Global Theme…"
msgstr "Sélectionner un thème global..."

#: contents/ui/ToolBoxContent.qml:276
#, kde-format
msgid "Configure Display Settings…"
msgstr "Configurer les paramètres d'affichage…"

#: contents/ui/ToolBoxContent.qml:298
#, kde-format
msgid "Exit Edit Mode"
msgstr "Quitter le mode d'édition"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Terminer la personnalisation de la disposition"

#~ msgid "Default"
#~ msgstr "Par défaut"

#~ msgid "Desktop Toolbox"
#~ msgstr "Boîte à outils du bureau"

#~ msgid "Desktop Toolbox — %1 Activity"
#~ msgstr "Boîte à outils du bureau — Activité %1"

#~ msgid "Lock Screen"
#~ msgstr "Écran de verrouillage"

#~ msgid "Leave"
#~ msgstr "Quitter"
