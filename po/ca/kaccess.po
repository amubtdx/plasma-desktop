# Translation of kaccess.po to Catalan
# Copyright (C) 2000-2022 This_file_is_part_of_KDE
#
# Sebastià Pla i Sanz <sps@sastia.com>, 2000, 2001, 2005, 2006.
# Ivan Lloro Boada <antispam@wanadoo.es>, 2004.
# Albert Astals Cid <aacid@kde.org>, 2005.
# Josep M. Ferrer <txemaq@gmail.com>, 2007, 2008, 2009, 2010, 2011, 2013, 2015, 2016, 2017, 2020.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2012, 2015, 2016, 2022.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-18 00:46+0000\n"
"PO-Revision-Date: 2022-04-14 00:34+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sebastià Pla i Sanz"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sps@sastia.com"

#: kaccess.cpp:68
msgid ""
"The Shift key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"La tecla Maj ha estat bloquejada i ara està activa per a totes les tecles "
"que es premin a continuació."

#: kaccess.cpp:69
msgid "The Shift key is now active."
msgstr "La tecla de majúscules ara està activa."

#: kaccess.cpp:70
msgid "The Shift key is now inactive."
msgstr "La tecla de majúscules ara està inactiva."

#: kaccess.cpp:74
msgid ""
"The Control key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"La tecla Control ha estat bloquejada i ara està activa per a totes les "
"tecles que es premin a continuació."

#: kaccess.cpp:75
msgid "The Control key is now active."
msgstr "La tecla Control ara està activa."

#: kaccess.cpp:76
msgid "The Control key is now inactive."
msgstr "La tecla Control ara està inactiva."

#: kaccess.cpp:80
msgid ""
"The Alt key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"La tecla Alt ha estat bloquejada i ara està activa per a totes les tecles "
"que es premin a continuació."

#: kaccess.cpp:81
msgid "The Alt key is now active."
msgstr "La tecla Alt està activa."

#: kaccess.cpp:82
msgid "The Alt key is now inactive."
msgstr "La tecla Alt està inactiva."

#: kaccess.cpp:86
msgid ""
"The Win key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"La tecla de Windows ha estat fixada i està activa per a totes les tecles que "
"es premin a continuació."

#: kaccess.cpp:87
msgid "The Win key is now active."
msgstr "La tecla de Windows està activa."

#: kaccess.cpp:88
msgid "The Win key is now inactive."
msgstr "La tecla de Windows està inactiva."

#: kaccess.cpp:92
msgid ""
"The Meta key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"La tecla Meta ha estat fixada i està activa per a totes les tecles que es "
"premin a continuació."

#: kaccess.cpp:93
msgid "The Meta key is now active."
msgstr "La tecla Meta està activa."

#: kaccess.cpp:94
msgid "The Meta key is now inactive."
msgstr "La tecla Meta està inactiva."

#: kaccess.cpp:98
msgid ""
"The Super key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"La tecla Súper ha estat fixada i està activa per a totes les tecles que es "
"premin a continuació."

#: kaccess.cpp:99
msgid "The Super key is now active."
msgstr "La tecla Súper està activa."

#: kaccess.cpp:100
msgid "The Super key is now inactive."
msgstr "La tecla Súper està inactiva."

#: kaccess.cpp:104
msgid ""
"The Hyper key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"La tecla Hiper ha estat fixada i està activa per a totes les tecles que es "
"premin a continuació."

#: kaccess.cpp:105
msgid "The Hyper key is now active."
msgstr "La tecla Hiper està activa."

#: kaccess.cpp:106
msgid "The Hyper key is now inactive."
msgstr "La tecla Hiper està inactiva."

#: kaccess.cpp:110
msgid ""
"The Alt Graph key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"La tecla Alt Gr ha estat fixada i està activa per a totes les tecles que es "
"premin a continuació."

#: kaccess.cpp:111
msgid "The Alt Graph key is now active."
msgstr "La tecla Alt Gr ara està activa."

#: kaccess.cpp:112
msgid "The Alt Graph key is now inactive."
msgstr "La tecla Alt Gr ara està inactiva."

#: kaccess.cpp:113
msgid "The Num Lock key has been activated."
msgstr "S'ha activat la tecla Bloq Núm."

#: kaccess.cpp:113
msgid "The Num Lock key is now inactive."
msgstr "La tecla Bloq Núm ara està inactiva."

#: kaccess.cpp:114
msgid "The Caps Lock key has been activated."
msgstr "S'ha activat la tecla Bloq Maj."

#: kaccess.cpp:114
msgid "The Caps Lock key is now inactive."
msgstr "La tecla Bloq Maj ara està inactiva."

#: kaccess.cpp:115
msgid "The Scroll Lock key has been activated."
msgstr "La tecla Bloq Despl s'ha activat."

#: kaccess.cpp:115
msgid "The Scroll Lock key is now inactive."
msgstr "La tecla Bloq Despl ara està inactiva."

#: kaccess.cpp:331
#, kde-format
msgid "Toggle Screen Reader On and Off"
msgstr "Engega/apaga el lector de pantalla"

#: kaccess.cpp:333
#, kde-format
msgctxt "Name for kaccess shortcuts category"
msgid "Accessibility"
msgstr "Accessibilitat"

#: kaccess.cpp:619
#, kde-format
msgid "AltGraph"
msgstr "AltGr"

#: kaccess.cpp:621
#, kde-format
msgid "Hyper"
msgstr "Hiper"

#: kaccess.cpp:623
#, kde-format
msgid "Super"
msgstr "Súper"

#: kaccess.cpp:625
#, kde-format
msgid "Meta"
msgstr "Meta"

#: kaccess.cpp:642
#, kde-format
msgid "Warning"
msgstr "Avís"

#: kaccess.cpp:670
#, kde-format
msgid "&When a gesture was used:"
msgstr "&En usar un gest:"

#: kaccess.cpp:676
#, kde-format
msgid "Change Settings Without Asking"
msgstr "Canvia l'arranjament sense preguntar"

#: kaccess.cpp:677
#, kde-format
msgid "Show This Confirmation Dialog"
msgstr "Mostra aquest diàleg de confirmació"

#: kaccess.cpp:678
#, kde-format
msgid "Deactivate All AccessX Features & Gestures"
msgstr "Desactiva totes les característiques i gestos AccessX"

#: kaccess.cpp:721 kaccess.cpp:723
#, kde-format
msgid "Slow keys"
msgstr "Tecles lentes"

#: kaccess.cpp:726 kaccess.cpp:728
#, kde-format
msgid "Bounce keys"
msgstr "Tecles de repetició"

#: kaccess.cpp:731 kaccess.cpp:733
#, kde-format
msgid "Sticky keys"
msgstr "Tecles apegaloses"

#: kaccess.cpp:736 kaccess.cpp:738
#, kde-format
msgid "Mouse keys"
msgstr "Tecles de ratolí"

#: kaccess.cpp:745
#, kde-format
msgid "Do you really want to deactivate \"%1\"?"
msgstr "De veres voleu desactivar «%1»?"

#: kaccess.cpp:748
#, kde-format
msgid "Do you really want to deactivate \"%1\" and \"%2\"?"
msgstr "De veres voleu desactivar «%1» i «%2»?"

#: kaccess.cpp:752
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\" and \"%3\"?"
msgstr "De veres voleu desactivar «%1», «%2» i «%3»?"

#: kaccess.cpp:755
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "De veres voleu desactivar «%1», «%2», «%3» i «%4»?"

#: kaccess.cpp:766
#, kde-format
msgid "Do you really want to activate \"%1\"?"
msgstr "De veres voleu activar «%1»?"

#: kaccess.cpp:769
#, kde-format
msgid "Do you really want to activate \"%1\" and to deactivate \"%2\"?"
msgstr "De veres voleu activar «%1» i desactivar «%2»?"

#: kaccess.cpp:772
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\" and \"%3\"?"
msgstr "De veres voleu activar «%1» i desactivar «%2» i «%3»?"

#: kaccess.cpp:778
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\", \"%3\" and "
"\"%4\"?"
msgstr "De veres voleu activar «%1» i desactivar «%2», «%3» i «%4»?"

#: kaccess.cpp:789
#, kde-format
msgid "Do you really want to activate \"%1\" and \"%2\"?"
msgstr "De veres voleu activar «%1» i «%2»?"

#: kaccess.cpp:792
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and \"%2\" and to deactivate \"%3\"?"
msgstr "De veres voleu activar «%1» i «%2» i desactivar «%3»?"

#: kaccess.cpp:798
#, kde-format
msgid ""
"Do you really want to activate \"%1\", and \"%2\" and to deactivate \"%3\" "
"and \"%4\"?"
msgstr "De veres voleu activar «%1» i «%2» i desactivar «%3» i «%4»?"

#: kaccess.cpp:809
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\" and \"%3\"?"
msgstr "De veres voleu activar «%1», «%2» i «%3»?"

#: kaccess.cpp:812
#, kde-format
msgid ""
"Do you really want to activate \"%1\", \"%2\" and \"%3\" and to deactivate "
"\"%4\"?"
msgstr "De veres voleu activar «%1», «%2» i «%3» i desactivar «%4»?"

#: kaccess.cpp:821
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "De veres voleu activar «%1», «%2», «%3» i «%4»?"

#: kaccess.cpp:830
#, kde-format
msgid "An application has requested to change this setting."
msgstr "Una aplicació ha requerit canviar aquest valor."

#: kaccess.cpp:834
#, kde-format
msgid ""
"You held down the Shift key for 8 seconds or an application has requested to "
"change this setting."
msgstr ""
"Heu premut la tecla Maj durant 8 segons o bé una aplicació ha demanat "
"canviar aquest valor."

#: kaccess.cpp:836
#, kde-format
msgid ""
"You pressed the Shift key 5 consecutive times or an application has "
"requested to change this setting."
msgstr ""
"Heu premut la tecla Maj 5 cops consecutius o bé una aplicació ha demanat "
"canviar aquest valor."

#: kaccess.cpp:840
#, kde-format
msgid "You pressed %1 or an application has requested to change this setting."
msgstr "Heu premut %1 o bé una aplicació ha demanat canviar aquest valor."

#: kaccess.cpp:845
#, kde-format
msgid ""
"An application has requested to change these settings, or you used a "
"combination of several keyboard gestures."
msgstr ""
"Una aplicació ha demanat canviar aquests valors, o bé heu usat una "
"combinació de diversos gestos del teclat."

#: kaccess.cpp:847
#, kde-format
msgid "An application has requested to change these settings."
msgstr "Una aplicació ha demanat canviar aquests valors."

#: kaccess.cpp:852
#, kde-format
msgid ""
"These AccessX settings are needed for some users with motion impairments and "
"can be configured in the KDE System Settings. You can also turn them on and "
"off with standardized keyboard gestures.\n"
"\n"
"If you do not need them, you can select \"Deactivate all AccessX features "
"and gestures\"."
msgstr ""
"Aquests valors AccessX són necessaris per a alguns usuaris amb dificultats "
"de moviment i poden configurar-se en el Centre de control del KDE. També els "
"podeu activar i desactivar amb gestos del teclat estandarditzats.\n"
"\n"
"Si no us calen, podeu seleccionar «Desactiva totes les característiques i "
"moviments AccessX»."

#: kaccess.cpp:873
#, kde-format
msgid ""
"Slow keys has been enabled. From now on, you need to press each key for a "
"certain length of time before it gets accepted."
msgstr ""
"S'han activat les tecles lentes. D'ara endavant, heu de prémer cada tecla "
"durant un cert temps abans que sigui acceptada."

#: kaccess.cpp:875
#, kde-format
msgid "Slow keys has been disabled."
msgstr "S'han desactivat les tecles lentes."

#: kaccess.cpp:879
#, kde-format
msgid ""
"Bounce keys has been enabled. From now on, each key will be blocked for a "
"certain length of time after it was used."
msgstr ""
"S'han activat les tecles de repetició. D'ara endavant, cada tecla es "
"bloquejarà durant un cert temps després d'usar-se."

#: kaccess.cpp:881
#, kde-format
msgid "Bounce keys has been disabled."
msgstr "S'han desactivat les tecles de repetició."

#: kaccess.cpp:885
#, kde-format
msgid ""
"Sticky keys has been enabled. From now on, modifier keys will stay latched "
"after you have released them."
msgstr ""
"S'han activat les tecles apegaloses. D'ara endavant, les tecles "
"modificadores romandran fixades després d'haver-les premut."

#: kaccess.cpp:887
#, kde-format
msgid "Sticky keys has been disabled."
msgstr "S'han desactivat les tecles apegaloses."

#: kaccess.cpp:891
#, kde-format
msgid ""
"Mouse keys has been enabled. From now on, you can use the number pad of your "
"keyboard in order to control the mouse."
msgstr ""
"S'han activat les tecles de ratolí. D'ara endavant, podeu usar el teclat "
"numèric per a controlar el ratolí."

#: kaccess.cpp:893
#, kde-format
msgid "Mouse keys has been disabled."
msgstr "S'han desactivat les tecles de ratolí."

#: main.cpp:49
#, kde-format
msgid "Accessibility"
msgstr "Accessibilitat"

#: main.cpp:49
#, kde-format
msgid "(c) 2000, Matthias Hoelzer-Kluepfel"
msgstr "(c) 2000, Matthias Hoelzer-Kluepfel"

#: main.cpp:51
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Matthias Hoelzer-Kluepfel"

#: main.cpp:51
#, kde-format
msgid "Author"
msgstr "Autor"

#~ msgid "KDE Accessibility Tool"
#~ msgstr "Eina d'accessibilitat del KDE"

#~ msgid "kaccess"
#~ msgstr "kaccess"
