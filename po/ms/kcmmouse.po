# kcminput Bahasa Melayu (Malay) (ms)
# Copyright (C) 2008, 2009 K Desktop Environment
#
# Muhammad Najmi Ahmad Zabidi <md_najmi@yahoo.com>, 2003.
# Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>, 2004, 2009, 2010.
# Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-20 00:51+0000\n"
"PO-Revision-Date: 2011-01-12 20:48+0800\n"
"Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>\n"
"Language-Team: Malay <kedidiemas@yahoogroups.com>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sharuzzaman Ahmat Raslan"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sharuzzaman@myrealbox.com"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:36
#, kde-format
msgid "Pointer device KCM"
msgstr ""

#: kcm/libinput/libinput_config.cpp:38
#, kde-format
msgid "System Settings module for managing mice and trackballs."
msgstr ""

#: kcm/libinput/libinput_config.cpp:40
#, kde-format
msgid "Copyright 2018 Roman Gilg"
msgstr ""

#: kcm/libinput/libinput_config.cpp:43 kcm/xlib/xlib_config.cpp:91
#, kde-format
msgid "Roman Gilg"
msgstr ""

#: kcm/libinput/libinput_config.cpp:43
#, kde-format
msgid "Developer"
msgstr ""

#: kcm/libinput/libinput_config.cpp:109
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:114
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:125
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:145
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:167
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:191
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:193
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:79
#, kde-format
msgid "Device:"
msgstr ""

#: kcm/libinput/main.qml:103 kcm/libinput/main_deviceless.qml:54
#, fuzzy, kde-format
#| msgid "&General"
msgid "General:"
msgstr "U&mum"

#: kcm/libinput/main.qml:105
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:124
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:130 kcm/libinput/main_deviceless.qml:56
#, fuzzy, kde-format
#| msgid "Le&ft handed"
msgid "Left handed mode"
msgstr "Tangan K&iri"

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:75
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:155 kcm/libinput/main_deviceless.qml:81
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:174 kcm/libinput/main_deviceless.qml:100
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:184 kcm/libinput/main_deviceless.qml:110
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Pointer speed:"
msgstr "Ambang penuding:"

#: kcm/libinput/main.qml:216 kcm/libinput/main_deviceless.qml:142
#, fuzzy, kde-format
#| msgid "Acceleration &profile:"
msgid "Acceleration profile:"
msgstr "&Profil pecutan:"

#: kcm/libinput/main.qml:247 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Flat"
msgstr ""

#: kcm/libinput/main.qml:250 kcm/libinput/main_deviceless.qml:176
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:257 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Adaptive"
msgstr ""

#: kcm/libinput/main.qml:260 kcm/libinput/main_deviceless.qml:186
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:272 kcm/libinput/main_deviceless.qml:198
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/main.qml:274 kcm/libinput/main_deviceless.qml:200
#, fuzzy, kde-format
#| msgid "Re&verse scroll direction"
msgid "Invert scroll direction"
msgstr "&Patah balik arah skrol"

#: kcm/libinput/main.qml:289 kcm/libinput/main_deviceless.qml:215
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:295
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Scrolling speed:"
msgstr "Ambang penuding:"

#: kcm/libinput/main.qml:343
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/main.qml:349
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/main.qml:360
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:396
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:426
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:427
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:431
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:448
#, fuzzy, kde-format
#| msgid "Press Connect Button"
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "Tindakan Butang Sambung"

#: kcm/libinput/main.qml:449
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:478
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""
"<h1>Tetikus</h1> Modul ini membenarkan anda untuk memilih pelbagai pilihan "
"untuk pengendalian peranti penunjuk anda. Peranti penunjuk boleh jadi "
"tetikus, bebola trek, atau perkakasan lain yang menawarkan fungsi yang sama."

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "U&mum"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""
"Jika anda kidal, anda mungkin lebih gemar menyaling tukar fungsi butang kiri "
"dengan kanan pada peranti tuding anda dengan memilih opsyen 'kidal'. Jika "
"peranti tuding anda mempunyai lebih daripada dua butang, hanya yang "
"berfungsi sebagai butang kiri dan kanan akan terlibat. Misalnya, jika anda "
"mempunyai tetikus tiga butang, butang tengah tidak terlibat."

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr "Tertib Butang"

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, kde-format
msgid "Righ&t handed"
msgstr "&Tangan Kanan"

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr "Tangan K&iri"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr ""
"Ubah arah penskrolan bagi reroda tetikus atau butang tetikus ke-4 dan ke-5."

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr "&Patah balik arah skrol"

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "Lanjutan"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr "Pecutan penunjuk:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr "Ambang penuding:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr "Senggang klik dua kali:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr "Masa mula tarik:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr "Jarak-mula tarik:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr "Bola tetikus skrol dengan:"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""
"<p>Pilihan ini membolehkan anda mengubah hubungan antara jarak penuding "
"tetikus bergerak di skrin dan pergerakan relatif peranti fizikal itu sendiri "
"(yang boleh jadi tetikus, bebola trek, atau peranti tuding lain.)</"
"p><p>Nilai pecutan yang tinggi akan membawa kepada pergerakan besar penuding "
"tetikus di skrin sekalipun apabila anda hanya membuat pergerakan kecil "
"dengan peranti fizikal. Memilih nilai yang sangat tinggi boleh menyebabkan "
"penuding tetikus terbang merentasi skrin, menyebabkannya sukar dikawal.</p>"

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr " x"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""
"<p>Ambang ialah jarak terkecil penuding tetikus mesti bergerak di skrin "
"sebelum pecutan berfungsi. Jika pergerakan lebih kecil daripada ambang. "
"penuding tetikus bergerak seolah-olah pecutan ditetapkan kepada 1X;</p><p> "
"justeru apabila anda membuat pergerakan kecil dengan peranti fizikal, tiada "
"pecutan langsung, memberikan anda darjah kawalan yang lebih besar ke atas "
"penuding tetikus. Dengan pergerakan lebih besar bagi peranti fizikal, anda "
"boleh gerakkan penuding tetikus dengan pantas ke kawasan berbeza di skrin.</"
"p>"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""
"Klik dua kali jeda ialah masa maksimum (dalam milisaat) di antara dua klik "
"tetikus yang mengubahnya kepada klik dua kali. Jika klik kedua berlaku lewat "
"daripada jeda masa ini selepas klik pertama, ia dicam sebagai klik yang "
"berasingan."

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr " msec"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""
"Jika anda klik dengan tetikus (misalnya editor berbilang baris) dan mula "
"menggerakkan tetikus dalam masa mula seret, operasi seret akan dimulakan."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""
"Jika anda klik tetikus dan mula menggerakkan tetikus sekurang-kurangnya "
"jarak mula seret, operasi seret akan dimulakan."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""
"Jika anda menggunakan reroda tetikus, nilai ini menentukan bilangan baris "
"untuk skrol bagi setiap pergerakan reroda. Perhatikan bahawa bilangan ini "
"melebihi bilangan baris dapat dilihat, ia akan diabaikan dan pergerakan "
"reroda akan dikendalikan sebagai pergerakan ke atas/ke bawah."

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, fuzzy, kde-format
#| msgid "Mouse Navigation"
msgid "Keyboard Navigation"
msgstr "Navigasi Tetikus"

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr "Alihkan penunjuk dengan papan kekunci (guna pad no&m)"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr "&Tempo pecutan"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, kde-format
msgid "R&epeat interval:"
msgstr "Ulang s&enggang:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr "Masa pecu&tan :"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, kde-format
msgid "Ma&ximum speed:"
msgstr "Kelaj&uan Maksima:"

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, kde-format
msgid " pixel/sec"
msgstr " piksel/saat"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr "&Profil pecutan:"

#: kcm/xlib/xlib_config.cpp:79
#, kde-format
msgid "Mouse"
msgstr "Tetikus"

#: kcm/xlib/xlib_config.cpp:83
#, fuzzy, kde-format
#| msgid "(c) 1997 - 2005 Mouse developers"
msgid "(c) 1997 - 2018 Mouse developers"
msgstr "(c) 1997 - 2005 Mouse developers"

#: kcm/xlib/xlib_config.cpp:84
#, kde-format
msgid "Patrick Dowler"
msgstr "Patrick Dowler"

#: kcm/xlib/xlib_config.cpp:85
#, kde-format
msgid "Dirk A. Mueller"
msgstr "Dirk A. Mueller"

#: kcm/xlib/xlib_config.cpp:86
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: kcm/xlib/xlib_config.cpp:87
#, kde-format
msgid "Bernd Gehrmann"
msgstr "Bernd Gehrmann"

#: kcm/xlib/xlib_config.cpp:88
#, kde-format
msgid "Rik Hemsley"
msgstr "Rik Hemsley"

#: kcm/xlib/xlib_config.cpp:89
#, kde-format
msgid "Brad Hughes"
msgstr "Brad Hughes"

#: kcm/xlib/xlib_config.cpp:90
#, kde-format
msgid "Brad Hards"
msgstr "Brad Hards"

#: kcm/xlib/xlib_config.cpp:283 kcm/xlib/xlib_config.cpp:288
#, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] " piksel"
msgstr[1] " piksel"

#: kcm/xlib/xlib_config.cpp:293
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] ""
msgstr[1] ""

#~ msgid "Ralf Nolden"
#~ msgstr "Ralf Nolden"

#, fuzzy
#~| msgid "Acceleration &time:"
#~ msgid "Acceleration:"
#~ msgstr "Masa pecu&tan :"

#, fuzzy
#~| msgid "Acceleration &profile:"
#~ msgid "Acceleration Profile:"
#~ msgstr "&Profil pecutan:"

#~ msgid "Icons"
#~ msgstr "Ikon"

#~ msgid ""
#~ "The default behavior in KDE is to select and activate icons with a single "
#~ "click of the left button on your pointing device. This behavior is "
#~ "consistent with what you would expect when you click links in most web "
#~ "browsers. If you would prefer to select with a single click, and activate "
#~ "with a double click, check this option."
#~ msgstr ""
#~ "Ragam default KDE ialah dengan memilih dan mengaktifkan ikon dengan klik "
#~ "sekali butang kiri peranti penunjuk anda. Ragam ini konsisten dengan apa "
#~ "yang anda jangka apabila anda klik pautan di dalam kebanyakan pelayar "
#~ "web. Jika anda suka 'pilih' dengan klik sekali, dan untuk 'mengaktifkan' "
#~ "dengan klik dua kali, tanda pilihan ini."

#~ msgid ""
#~ "Dou&ble-click to open files and folders (select icons on first click)"
#~ msgstr ""
#~ "Klik dua kali untuk &buka fail dan folder (pilih ikon pada klik pertama)"

#~ msgid "Activates and opens a file or folder with a single click."
#~ msgstr "Aktifkan dan buka fail atau folder dengan sekali klik."

#~ msgid "&Single-click to open files and folders"
#~ msgstr "Klik &sekali untuk buka fail dan folder"

#~ msgid "Select the cursor theme you want to use:"
#~ msgstr "Pilih tema kursor yang anda ingin guna:"

#~ msgid "Name"
#~ msgstr "Nama"

#~ msgid "Description"
#~ msgstr "Huraian"

#~ msgid "You have to restart KDE for these changes to take effect."
#~ msgstr "Anda perlu mulakan semula KDE agar perubahan ini berkesan."

#~ msgid "Cursor Settings Changed"
#~ msgstr "Seting Kursor Diubah"

#~ msgid "Small black"
#~ msgstr "Hitam kecil"

#~ msgid "Small black cursors"
#~ msgstr "Kursor hitam kecil"

#~ msgid "Large black"
#~ msgstr "Hitam besar"

#~ msgid "Large black cursors"
#~ msgstr "Kursor hitam besar"

#~ msgid "Small white"
#~ msgstr "Putih kecil"

#~ msgid "Small white cursors"
#~ msgstr "Kursor putih kecil"

#~ msgid "Large white"
#~ msgstr "Putih besar"

#~ msgid "Large white cursors"
#~ msgstr "Kursor putih besar"

#~ msgid "Cha&nge pointer shape over icons"
#~ msgstr "Tukar bentuk penunjuk di atas iko&n"

#~ msgid "A&utomatically select icons"
#~ msgstr "Pilih ikon secara a&utomatik"

#~ msgctxt "label. delay (on milliseconds) to automatically select icons"
#~ msgid "Delay"
#~ msgstr "Penangguhan"

#~ msgctxt "milliseconds. time to automatically select the items"
#~ msgid " ms"
#~ msgstr " ms"

#~ msgid ""
#~ "If you check this option, pausing the mouse pointer over an icon on the "
#~ "screen will automatically select that icon. This may be useful when "
#~ "single clicks activate icons, and you want only to select the icon "
#~ "without activating it."
#~ msgstr ""
#~ "Jika anda tanda pilihan ini, meletakkan penunjuk tetikus pada ikon di "
#~ "skrin akan secara automatik memilih ikon. Ini berguna apabila klik sekali "
#~ "mengaktifkan ikon, dan anda hanya mahukan memilih ikon tanpa mengaktifkan "
#~ "ikon. "

#~ msgid ""
#~ "If you have checked the option to automatically select icons, this slider "
#~ "allows you to select how long the mouse pointer must be paused over the "
#~ "icon before it is selected."
#~ msgstr ""
#~ "Jika anda memilih pilihan untuk automatik pilih ikon, peluncur "
#~ "membenarkan anda untuk memilih berapa lama penunjuk tetikus perlu berada "
#~ "pada ikon sebelum dipilih."

#~ msgid "Mouse type: %1"
#~ msgstr "Jenis tetikus: %1"

#~ msgid ""
#~ "RF channel 1 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "Saluran 1 RF telah diset. Tekan butang Sambung pada tetikus untuk "
#~ "mewujudkan semula pautan"

#~ msgid ""
#~ "RF channel 2 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "Saluran 2 RF telah diset. Tekan butang Sambung pada tetikus untuk "
#~ "mewujudkan semula pautan"

#~ msgctxt "no cordless mouse"
#~ msgid "none"
#~ msgstr "tiada"

#~ msgid "Cordless Mouse"
#~ msgstr "Tetikus Tanpa Kord"

#~ msgid "Cordless Wheel Mouse"
#~ msgstr "Tetikus Reroda Tanpa Kord"

#~ msgid "Cordless MouseMan Wheel"
#~ msgstr "Reroda MouseMan Tanpa Kord"

#~ msgid "Cordless TrackMan Wheel"
#~ msgstr "Reroda TrackMan Tanpa Kord"

#~ msgid "TrackMan Live"
#~ msgstr "TrackMan Live"

#~ msgid "Cordless TrackMan FX"
#~ msgstr "TrackMan FX Tanpa Kord"

#~ msgid "Cordless MouseMan Optical"
#~ msgstr "MouseMan Optik Tanpa Kord"

#~ msgid "Cordless Optical Mouse"
#~ msgstr "Tetikus Optik Tanpa Kord"

#~ msgid "Cordless MouseMan Optical (2ch)"
#~ msgstr "MouseMan Optik (2ch) Tanpa Kord"

#~ msgid "Cordless Optical Mouse (2ch)"
#~ msgstr "Tetikus Optik (2ch) Tanpa Kord"

#~ msgid "Cordless Mouse (2ch)"
#~ msgstr "Tetikus Tanpa Kod (2ch)"

#~ msgid "Cordless Optical TrackMan"
#~ msgstr "TrackMan Optik Tanpa Kod"

#~ msgid "MX700 Cordless Optical Mouse"
#~ msgstr "MX700 Tetikus Optik Tanpa Kod"

#~ msgid "MX700 Cordless Optical Mouse (2ch)"
#~ msgstr "MX700 Tetikus Optik Tanpa Kod (2ch)"

#~ msgid "Unknown mouse"
#~ msgstr "Tetikus tak diketahui"

#~ msgid "Cordless Name"
#~ msgstr "Nama Tanpa Kord"

#~ msgid "Sensor Resolution"
#~ msgstr "Resolusi Penderia"

#~ msgid "400 counts per inch"
#~ msgstr "400 kiraan per inci"

#~ msgid "800 counts per inch"
#~ msgstr "800 kiraan per inci"

#~ msgid "Battery Level"
#~ msgstr "Paras Bateri"

#~ msgid "RF Channel"
#~ msgstr "Saluran RF"

#~ msgid "Channel 1"
#~ msgstr "Saluran 1"

#~ msgid "Channel 2"
#~ msgstr "Saluran 2"

#~ msgid ""
#~ "You have a Logitech Mouse connected, and libusb was found at compile "
#~ "time, but it was not possible to access this mouse. This is probably "
#~ "caused by a permissions problem - you should consult the manual on how to "
#~ "fix this."
#~ msgstr ""
#~ "Anda ada Tetikus Logitech yang disambungkan, dan libusb ditemui pada masa "
#~ "kompil, tetapi tidak mungkin untuk mengakses tetikus ini. Ini mungkin "
#~ "disebabkan oleh masalah keizinan - anda perlu rujuk manual tentang "
#~ "bagaimana hendak membetulkannya."

#~ msgid "Cursor Theme"
#~ msgstr "Tema Kursor"

#~ msgid "(c) 2003-2007 Fredrik Höglund"
#~ msgstr "(c) 2003-2007 Fredrik Höglund"

#~ msgid "Fredrik Höglund"
#~ msgstr "Fredrik Höglund"

#~ msgid "Drag or Type Theme URL"
#~ msgstr "Seret atau Taip URL Tema"

#~ msgid "Unable to find the cursor theme archive %1."
#~ msgstr "Tidak dapat mencari arkib tema kursor %1."

#~ msgid ""
#~ "Unable to download the cursor theme archive; please check that the "
#~ "address %1 is correct."
#~ msgstr ""
#~ "Tidak dapat memuat turun arkib tema kursor; pastikan alamat %1 adalah "
#~ "betul."

#~ msgid "The file %1 does not appear to be a valid cursor theme archive."
#~ msgstr "Fail %1 bukannya arkib tema kursor yang sah."

#~ msgid ""
#~ "<qt>You cannot delete the theme you are currently using.<br />You have to "
#~ "switch to another theme first.</qt>"
#~ msgstr ""
#~ "<qt>Anda tidak boleh memadam tema yang anda sedang gunakan.<br />Anda "
#~ "perlu untuk menukar ke tema lain terlebih dahulu.</qt>"

#~ msgid ""
#~ "<qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This "
#~ "will delete all the files installed by this theme.</qt>"
#~ msgstr ""
#~ "<qt>Anda pasti anda ingin membuang tema kursor <i>%1</i> ?<br />Ini akan "
#~ "memadamkan semua fail yang dipasang oleh tema ini.</qt>"

#~ msgid "Confirmation"
#~ msgstr "Kepastian"

#~ msgid ""
#~ "A theme named %1 already exists in your icon theme folder. Do you want "
#~ "replace it with this one?"
#~ msgstr ""
#~ "Tema bernama %1 sudah wujud dalam folder tema ikon anda. Anda ingin "
#~ "menggantikannya dengan yang ini?"

#~ msgid "Overwrite Theme?"
#~ msgstr "Tulisganti Tema?"

#~ msgid ""
#~ "Select the cursor theme you want to use (hover preview to test cursor):"
#~ msgstr ""
#~ "Pilih tema kursor yang anda ingin guna (prapapar ambang untuk menguji "
#~ "kursor):"

#, fuzzy
#~| msgid "Install New Theme..."
#~ msgid "Get New Theme..."
#~ msgstr "Pasang Tema Baru..."

#, fuzzy
#~| msgid "Install New Theme..."
#~ msgid "Install From File..."
#~ msgstr "Pasang Tema Baru..."

#~ msgid "Remove Theme"
#~ msgstr "Buang Tema"

#~ msgid "KDE Classic"
#~ msgstr "KDE Klasik"

#~ msgid "The default cursor theme in KDE 2 and 3"
#~ msgstr "Tema kursor default dalam KDE 2 dan 3"

#~ msgid "No description available"
#~ msgstr "Tidak keterangan didapati"
