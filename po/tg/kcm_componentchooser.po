# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-09 00:48+0000\n"
"PO-Revision-Date: 2019-09-17 14:18+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: English <kde-i18n-doc@kde.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#. i18n: ectx: label, entry (browserApplication), group (General)
#: browser_settings.kcfg:9
#, kde-format
msgid "Default web browser"
msgstr ""

#: componentchooser.cpp:74 componentchooserterminal.cpp:74
#, kde-format
msgid "Other…"
msgstr ""

#: componentchooserbrowser.cpp:18
#, kde-format
msgid "Select default browser"
msgstr ""

#: componentchooseremail.cpp:19
#, kde-format
msgid "Select default e-mail client"
msgstr ""

#: componentchooserfilemanager.cpp:14
#, kde-format
msgid "Select default file manager"
msgstr ""

#: componentchoosergeo.cpp:11
#, kde-format
msgid "Select default map"
msgstr ""

#: componentchoosertel.cpp:15
#, kde-format
msgid "Select default dialer application"
msgstr ""

#: componentchooserterminal.cpp:24
#, kde-format
msgid "Select default terminal emulator"
msgstr ""

#: package/contents/ui/main.qml:30
#, kde-format
msgid "Web browser:"
msgstr ""

#: package/contents/ui/main.qml:42
#, kde-format
msgid "File manager:"
msgstr ""

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Email client:"
msgstr ""

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Terminal emulator:"
msgstr ""

#: package/contents/ui/main.qml:78
#, kde-format
msgid "Map:"
msgstr ""

#: package/contents/ui/main.qml:90
#, kde-format
msgctxt "Default phone app"
msgid "Dialer:"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Victor Ibragimov"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "victor.ibragimov@gmail.com"

#~ msgid "Unknown"
#~ msgstr "Номаълум"
