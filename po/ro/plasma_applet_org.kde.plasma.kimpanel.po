# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2009, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2022-02-04 15:42+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "Aspect"

#: package/contents/ui/ActionMenu.qml:93
#, kde-format
msgid "(Empty)"
msgstr "(gol)"

#: package/contents/ui/ConfigAppearance.qml:29
#, kde-format
msgid "Input method list:"
msgstr "Listă cu metode de introducere:"

#: package/contents/ui/ConfigAppearance.qml:30
#, kde-format
msgid "Vertical"
msgstr "Vertical"

#: package/contents/ui/ConfigAppearance.qml:36
#, kde-format
msgid "Horizontal"
msgstr "Orizontal"

#: package/contents/ui/ConfigAppearance.qml:43
#, kde-format
msgid "Font:"
msgstr "Font:"

#: package/contents/ui/ConfigAppearance.qml:49
#, kde-format
msgid "Use custom:"
msgstr "Folosește personalizat:"

#: package/contents/ui/ConfigAppearance.qml:55
#, kde-format
msgctxt "The selected font family and font size"
msgid " "
msgstr " "

#: package/contents/ui/ConfigAppearance.qml:67
#, kde-format
msgid "Select Font…"
msgstr "Alege fontul…"

#: package/contents/ui/ConfigAppearance.qml:73
#, kde-format
msgctxt "The arrangement of icons in the Panel"
msgid "Panel icon size:"
msgstr "Dimensiune pictograme panou:"

#: package/contents/ui/ConfigAppearance.qml:74
#, kde-format
msgid "Small"
msgstr "Mică"

#: package/contents/ui/ConfigAppearance.qml:81
#, kde-format
msgid "Scale with Panel height"
msgstr "Scalează cu înălțimea panoului"

#: package/contents/ui/ConfigAppearance.qml:82
#, kde-format
msgid "Scale with Panel width"
msgstr "Scalează cu lățimea panoului"

#: package/contents/ui/ConfigAppearance.qml:90
#, kde-format
msgctxt "@title:window"
msgid "Select Font"
msgstr "Alege fontul"

#: package/contents/ui/ContextMenu.qml:83
#, kde-format
msgid "Show"
msgstr "Arată"

#: package/contents/ui/ContextMenu.qml:98
#, kde-format
msgid "Hide %1"
msgstr "Ascunde %1"

#: package/contents/ui/ContextMenu.qml:107
#, kde-format
msgid "Configure Input Method"
msgstr "Configurează metoda de introducere"

#: package/contents/ui/ContextMenu.qml:115
#, kde-format
msgid "Reload Config"
msgstr "Reîncarcă configurările"

#: package/contents/ui/ContextMenu.qml:123
#, kde-format
msgid "Exit Input Method"
msgstr "Părăsește metoda de introducere"

#: package/contents/ui/main.qml:237
#, kde-format
msgid "Input Method Panel"
msgstr "Panou pentru metodă de introducere"

#~ msgid "Search"
#~ msgstr "Caută"

#~ msgid "Clear History"
#~ msgstr "Curăță istoricul"

#~ msgid "No recent Emojis"
#~ msgstr "Niciun emoji recent"

#~ msgid "%1 copied to the clipboard"
#~ msgstr "%1 copiat în clipboard"

#~ msgid "Recent"
#~ msgstr "Recente"

#~ msgid "All"
#~ msgstr "Toate"

#~ msgid "Categories"
#~ msgstr "Categorii"

#~ msgid "Search…"
#~ msgstr "Caută…"

#~ msgid "Search..."
#~ msgstr "Caută..."

#~ msgid "Select Font..."
#~ msgstr "Alege fontul..."

#, fuzzy
#~| msgid "KDE Input Method Panel"
#~ msgid "Start Input Method"
#~ msgstr "Panou pentru Metode de Intrare KDE"

#~ msgid "IM Panel Settings"
#~ msgstr "Configurări panou MI"

#~ msgid "kimpanel"
#~ msgstr "kimpanel"

#~ msgid "Embed into the panel"
#~ msgstr "Încorporează în panou"

#~ msgid "Copyright (C) 2009, Wang Hoi"
#~ msgstr "Drept de autor (C) 2009, Wang Hoi"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Sergiu Bivol"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sergiu@ase.md"

#~ msgid "Properties hidden in status bar"
#~ msgstr "Proprietăți ascunse în bara de stare"

#~ msgid "Position of floating status bar"
#~ msgstr "Poziția barei de stare flotante"

#~ msgid "Expand vertically."
#~ msgstr "Desfășoară vertical."

#~ msgid "Use multiple rows/cols."
#~ msgstr "Utilizează rânduri/coloane multiple."

#~ msgid "Put all words in one row."
#~ msgstr "Pune toate cuvintele într-un rând."

#~ msgid "Put all words in one col."
#~ msgstr "Pune toate cuvintele într-o coloană."

#~ msgid "Multiple rows/cols, but fixed rows."
#~ msgstr "Rânduri/coloane multiple, dar rânduri fixe."

#~ msgid "Multiple rows/cols, but fixed columns."
#~ msgstr "Rânduri/coloane multiple, dar coloane fixe."
