# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Zlatko Popov <zlatkopopov@fsa-bg.org>, 2006, 2007, 2008.
# Yasen Pramatarov <yasen@lindeas.com>, 2011, 2013.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcmcomponentchooser\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-09 00:48+0000\n"
"PO-Revision-Date: 2022-05-22 07:52+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <dict@ludost.net>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: label, entry (browserApplication), group (General)
#: browser_settings.kcfg:9
#, kde-format
msgid "Default web browser"
msgstr "Уеб браузър по подразбиране"

#: componentchooser.cpp:74 componentchooserterminal.cpp:74
#, kde-format
msgid "Other…"
msgstr "Други…"

#: componentchooserbrowser.cpp:18
#, kde-format
msgid "Select default browser"
msgstr "Избиране на браузър по подразбиране"

#: componentchooseremail.cpp:19
#, kde-format
msgid "Select default e-mail client"
msgstr "Избиране на пощенски клиент по подразбиране"

#: componentchooserfilemanager.cpp:14
#, kde-format
msgid "Select default file manager"
msgstr "Избиране на файлов мениджър по подразбиране"

#: componentchoosergeo.cpp:11
#, kde-format
msgid "Select default map"
msgstr "Избиране на карта по подразбиране"

#: componentchoosertel.cpp:15
#, kde-format
msgid "Select default dialer application"
msgstr "Избиране на приложение по подразбиране за телефонно набиране"

#: componentchooserterminal.cpp:24
#, kde-format
msgid "Select default terminal emulator"
msgstr "Избиране на терминален емулатор по подразбиране"

#: package/contents/ui/main.qml:30
#, kde-format
msgid "Web browser:"
msgstr "Преглед на интернет:"

#: package/contents/ui/main.qml:42
#, kde-format
msgid "File manager:"
msgstr "Управление на файлове:"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Email client:"
msgstr "Електронна поща:"

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Terminal emulator:"
msgstr "Терминален емулатор:"

#: package/contents/ui/main.qml:78
#, kde-format
msgid "Map:"
msgstr "Карта:"

#: package/contents/ui/main.qml:90
#, kde-format
msgctxt "Default phone app"
msgid "Dialer:"
msgstr "Телефонно набиране:"
